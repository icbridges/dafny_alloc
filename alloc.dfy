/*
OOB: Out-of-Bounds Access
DF: Double Free
UAF: Use-After-Free
ML: Memory Leak
*/

/*
The smallest chunk of memory available for use by the system. A Block is
analogous to a physical page.

used: whether the block is used as part of an allocation
head: whether the block marks the head of an allocation
*/
class Block {
  var used: bool
  var head: bool

  constructor() {
    used := false;
    head := true;
  }
}

/*
Initializes the heap.

heapSize: size of the initial heap

heap: a fresh heap snapshot
*/
method initHeap(heapSize: int) returns (heap: seq<Block>)
  // OOB: size of heap must be greater than 0
  requires heapSize > 0
  // OOB: heap size cannot change
  ensures |heap| == heapSize
{
  heap := [];
  var i := 0;
  while (i < heapSize)
    invariant i == |heap|
    invariant i <= heapSize
    decreases heapSize - i
  {
    var b := new Block();
    heap := heap + [b];
    i := i + 1;
  }
}

/*
Internal memory allocation function. This function performs all the
heavy-lifting required to create a new allocation

heap: the current snapshot of the heap
reqSize: the amount of memory requested

newHeap: an updated snapshot of the heap
allocStart: the offset into the heap to the start of the new allocation
*/
// Null-pointer dereference mitigation: allocStart is type int
method dafAlloc(heap: seq<Block>, reqSize: int) returns (newHeap: seq<Block>, allocStart: int)
  // OOB: requested size must stay within the bounds of the heap
  requires reqSize > 0
  requires reqSize <= |heap|
  // OOB: the size of original heap must be greater than 0
  requires |heap| > 0
  // OOB: The size of the heap does not change
  ensures |heap| == |newHeap|
{
  var i := 0;
  var freeCount := 0;
  allocStart := 0;
  var success := false;
  newHeap := [];

  // Iterate over the heap and find the first contiguous region of free memory
  // that is large enough to hold the the requested allocation
  while (i < |heap|)
    invariant i <= |heap|
    invariant i >= 0
    invariant freeCount >= 0
    invariant freeCount <= i
    invariant allocStart >= 0
    invariant allocStart < |heap|
    invariant (allocStart + reqSize) <= |heap|
    decreases |heap| - i
  {
    if (heap[i].used == false)
    {
      freeCount := freeCount + 1;
    }
    else {
      freeCount := 0;
    }

    // Check if the current range of free blocks is large enough
    if (freeCount == reqSize) {
      allocStart := i - (reqSize - 1);
      success := true;
      assert allocStart >= 0;
      break;
    }

    i := i + 1;
  }

  // Update the heap with the new allocation
  if (success == true)
  {
    newHeap := realloc(heap, allocStart, reqSize);
  }
  else {
    allocStart := -1;
    newHeap := heap;
  }

}

/*
Creates a new heap snapshot that is updated to account for the new allocation.
Since sequences are immutable, we must create a new one every time we update the
heap.

heap: the original snapshot of the heap
base: start address of the new allocation
count: size of the new allocation

newHeap: updated heap snapshot
*/
method realloc(heap: seq<Block>, base: int, count: int) returns (newHeap: seq<Block>)
  // OOB: requested size must stay within the bounds of the heap
  requires |heap| > 0
  requires base >= 0
  requires base < |heap|
  requires count > 0
  requires count <= |heap|
  requires base + count <= |heap|
  requires (base + count) <= |heap|
  // OOB: the size of the heap does not change
  ensures |newHeap| == |heap|
  // OOB: alloc can only touch the blocks in the heap that are being allocated
  ensures heap[..base] == newHeap[..base]
  ensures heap[(base + count)..] == newHeap[(base + count)..]
{

  newHeap := [];
  assert |newHeap| == 0;

  var header := createHeader();
  var body := allocateBody(count - 1);

  // Copy old heap data before our allocation
  if (base > 0) {
    newHeap := heap[0..base];
    assert |newHeap| == base;
  }

  newHeap := newHeap + [header];
  assert |newHeap| == base + 1;
  assert |newHeap| <= |heap|;

  newHeap := newHeap + body;
  assert |newHeap| == base + count;
  assert |newHeap| <= |heap|;

  // copy old heap data after our allocation
  if (|newHeap| < |heap|) {
    newHeap := newHeap + heap[|newHeap|..|heap|];
    assert |newHeap| == |heap|;
  }
  assert |newHeap| == |heap|;
}
/*
Internal function that does the heavy lifting for free-ing allocations

heap: snapshot of the old heap
base: address of the allocation

newHeap: new heap snapshot after the free has occurred
freeCount: the amount of memory freed. A value of zero indicates that a
failure has occurred.
*/
// Null pointer dereferece mitigation (base is type int)
method dafFree(heap: seq<Block>, base: int) returns (newHeap: seq<Block>, freeCount: int)
  // OOB: requested address must stay within the bounds of the heap
  requires |heap| > 0
  requires base >= 0
  requires base < |heap|
  ensures (base + freeCount) <= |heap|
  ensures freeCount >= 0
  // DF: the address based to free must point to the head of an allocation
  requires heap[base].head == true
  // OOB: the size of the heap cannot change
  ensures |heap| == |newHeap|
  // OOB: free can only touch the blocks in the heap that are being freed
  ensures heap[..base] == newHeap[..base]
  ensures heap[(base + freeCount)..] == newHeap[(base + freeCount)..]
{
  newHeap := [];
  var endIndex := getAllocSize(heap, base + 1);
  var freeBody := freeBodyChunk(endIndex - base);
  freeCount := |freeBody|;
  assert |freeBody| == endIndex - base;

  if (base > 0) {
    newHeap := heap[0..base];
    assert |newHeap| == base;
  }

  newHeap := newHeap + freeBody;
  assert |newHeap| == endIndex;
  assert |newHeap| <= |heap|;

  if (|newHeap| < |heap|) {
    newHeap := newHeap + heap[|newHeap|..|heap|];
    assert |newHeap| == |heap|;
  }
  assert |newHeap| == |heap|;
}

/*
Creates the new sequence slice that is used to mark a chunk of blocks as free

size: number of free blocks to create

freedChunk: new heap of free blocks
*/
method freeBodyChunk(size: int) returns (freedChunk: seq<Block>)
  requires size >= 0
  ensures |freedChunk| == size
{
  var i := 0;
  freedChunk := [];
  while (i < size)
    invariant i >= 0
    invariant i <= size
    invariant |freedChunk| == i
    decreases size - i
  {
    var block := new Block();
    block.head := true;
    block.used := false;
    freedChunk := freedChunk + [block];
    i := i + 1;
  }
}

/*
Counts the size of an allocation by iterating over the heap looking for the
next head marker

heap: current heap snapshot
base: offset of the allocation in the heap

end: offset of the last block within the allocation
*/
method getAllocSize(heap: seq<Block>, base: int) returns (end: int)
  // OOB: requested size must stay within the bounds of the heap
  requires |heap| > 0
  requires base >= 0
  requires base <= |heap|
  ensures end <= |heap|
  ensures end >= 0
  ensures end >= base
{
  end := base;
  while (end < |heap|)
    invariant end <= |heap|
    decreases |heap| - end
  {
    if (heap[end].head == true) {
      break;
    }
    end := end + 1;
  }
}

/*
Creates a new head node

header: the new head node
*/
method createHeader() returns (header: Block)
  ensures header.used == true
  // DF: ensures that all headers have the head flag set
  ensures header.head == true
{
  header := new Block();
  header.used := true;
  header.head := true;
}

/*
Creates a the body (without the head) of a new allocation

size: desired size of the alllocation body (size of the allocation - 1)

newBody: new allocation body
*/
method allocateBody(size: int) returns (newBody: seq<Block>)
  requires size >= 0
  ensures |newBody| == size
{
  var i := 0;
  newBody := [];
  while (i < size)
    invariant i >= 0
    invariant i <= size
    decreases size - i
    invariant |newBody| == i
  {
    var block := new Block();
    block.head := false;
    block.used := true;
    newBody := newBody + [block];
    i := i + 1;
    assert |newBody| == i;
  }
}

/* Interface for allocating memory
heap: current heap snapshot
size: requested allocation size

newHeap: updated heap snapshot
allocPtr: pointer (offset) to allocation within the updated heap
*/
method allocMem(heap: seq<Block>, size: int) returns (newHeap: seq<Block>, allocPtr: int)
{
  newHeap := [];
  allocPtr := -1;
  if (size > 0 && size <= |heap| && |heap| > 0) {
    newHeap, allocPtr := dafAlloc(heap, size);
  }
}

/* Interface for freeing memory
heap: current heap snapshot
base: pointer (offset) to allocation that you want to free

newHeap: updated heap snapshot
freed: amount of memory freed by this operation
*/
method freeMem(heap: seq<Block>, base: int) returns (newHeap: seq<Block>, freed: int)
{
  freed := 0;
  newHeap := heap[..];
  if ((base >= 0) && (base < |heap|) && (|heap| > 0)) {
    if (heap[base].head == true) {
    newHeap, freed := dafFree(heap, base);
    }
  }
}

/*
UAF and ML: Typically, the heap is maintained by external code and is
effectively treated as as a global resource. In an attempt to handle UAFs and
MLs, local copies of heap snapshots are passed around. This makes it so that
once a function returns, if it does not return a new heap snapshot, all
changes to the heap during the execution of the returning function are lost.
*/
method testFunc(heap: seq<Block>)
{
  // Create an allocation
  var freeSize: int;
  var newHeap, alloc1 := allocMem(heap, 2);

  // Do stuff here

  // Free the allocation
  newHeap, freeSize := freeMem(heap, alloc1);
}

method Main() {
  // Create a heap with ten "pages" of memory
  var heap := initHeap(10);
  // The caller's heap is not impacted by the callee's modifications to the heap
  testFunc(heap);
}
